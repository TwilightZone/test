#高中數學

# 數列級數
\\(1 + 2 + \dots + n = \frac{n(n + 1)}{2}\\)
\\(1^2 + 2^2 + 3^2 + \dots + n^2 = \frac{n(n + 1)(2n + 1)}{6} = \frac{n(n + \frac{1}{2})(n + 1)}{3}\\)
\\(1^3 + 2^3 + \dots + n^3 = \frac{n(n + 1)}{2}^2\\)

#指對數
* 任意數的0次方為1
* 0的非零任意數次方為0
* 0的零次方通常為undefine（黑洞？）

設\\(a > 0, b > 0, m , n \in \Bbb {R}\\)  

##指數率
\\(a^m \cdot a^n = a^{m + n}\\)  
\\(a^{m^n} = a^{mn}\\)  
\\((ab)^n = a^n b^n\\)  
\\(a^{-n} = \frac{1}{a^n}\\)  
\\(a^{\frac{m}{n}} = \sqrt[n]{a^m}\\) 

##對數律
\\(\log_a 1 = 0\\)
\\(\log_a a = 1\\)
\\(\log_a mn = \log_a m + \log_a n\\)
\\(\displaystyle \log_a \frac{m}{n} = \log_a m - \log_a n\\)
\\(\log_a x^m = m \log_a x\\)
\\(\log_a x = \frac{1}{\log_x a}\\)
換底公式 \\(\log_a{b}=\frac{{{\log }_{c}}b}{{{\log }_{c}}a} (c>0,\ c\ne 1) \\)

#三角函數
\\(\sin(-x) = -\sin{x}\\)
\\(\cos(-x) = \cos{x}\\)
\\(\sin(\frac{\pi}{2} - x) = \cos{x}\\)
\\(\cos(\frac{\pi}{2} - x) = \sin{x}\\)
\\(\sin(\frac{\pi}{2} + x) = \cos{x}\\)
\\(\cos(\frac{\pi}{2} + x) = -\sin{x}\\)
\\(\sin(\pi - x) = \sin{x}\\)
\\(\cos(\pi - x) = - \cos{x}\\)
\\(\sin(\pi + x) = - \sin{x}\\)
\\(\cos(\pi - x) = - \cos{x}\\)

\\(\sin^2 {x} + \cos^2 {x} = 1\\)
\\(\tan^2 {x} + 1 = \sec^2 {x}\\)
\\(\cot^2 {x} + 1 = \csc^2 {x}\\)

##和差化積
\\(\sin(\alpha + \beta) = \sin \alpha \cos \beta + \sin \beta \cos \alpha\\)
\\(\sin(\alpha - \beta) = \sin \alpha \cos \beta - \sin \beta \cos \alpha\\)
\\(\cos(\alpha + \beta) = \cos \alpha \cos \beta - \sin \alpha \sin \beta \\)
\\(\cos(\alpha - \beta) = \cos \alpha \cos \beta + \sin \alpha \sin \beta \\)
\\(\displaystyle \tan(\alpha + \beta) = \frac{\tan \alpha + \tan \beta}{1 - \tan \alpha \tan \beta}\\)
\\(\displaystyle \tan(\alpha - \beta) = \frac{\tan \alpha - \tan \beta}{1 + \tan \alpha \tan \beta}\\)

##倍角半倍角
\\(\sin{2 \alpha} = 2 \sin \alpha \cos \alpha\\)
\\(\cos{2 \alpha} = \cos^2 {x} - \sin^2 {x} = 2 \cos^2 {x} - 1 = 1 - 2 \sin^2 {x}\\)
\\(\displaystyle \tan{2 \alpha} = \frac{2 \tan \alpha}{1 - \tan^2 \alpha}\\)
\\(\displaystyle \sin{\frac{\alpha}{2}} = \pm \sqrt{\frac{1 - \cos \alpha}{2}}\\) (determine whether it is + or - by finding the quadrant that \\(\frac{\alpha}{2}\\) lies in)
\\(\displaystyle \cos{\frac{\alpha}{2}} = \pm \sqrt{\frac{1 + \cos \alpha}{2}}\\) (same as above)  
\\(\displaystyle \tan{\frac{\alpha}{2}} = \frac{1 - \cos \alpha}{\sin \alpha} = \frac{\sin \alpha}{1 + \cos \alpha}\\)